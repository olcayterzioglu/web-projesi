﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace YemekSitesi.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Ye()
        {
            ViewBag.Message = "Yiyecekler buraya.";

            return View();
        }

        public ActionResult İç()
        {
            ViewBag.Message = "İçecekler buraya.";

            return View();
        }
        public ActionResult İzle()
        {
            ViewBag.Message = "Yiyecek içecek videoları buraya.";

            return View();
        }

        public ActionResult Hakkımızda()
        {
            ViewBag.Message = "İletişim bilgileri vs.";

            return View();
        }
    }
}